package com.gorniak.yourcinema.tools;

import com.gorniak.yourcinema.comment.Comment;
import com.gorniak.yourcinema.image.Image;
import com.gorniak.yourcinema.movie.Movie;

public class InputValidator {

    public static void validateStringValue(String value) {
        validateObjectValue(value);
        if (value.isEmpty()) {
            throw new RuntimeException(); //todo: add custom exception with exception handler
        }
    }

    public static void validateNumberValue(Number value) {
        validateObjectValue(value);
        if (value.longValue() <= 0 || value.intValue() <= 0) {
            throw new RuntimeException(); //todo
        }
    }

    public static void validateCommentObject(Comment comment) {
        validateObjectValue(comment);
        validateStringValue(comment.getAuthor());
        validateStringValue(comment.getMessage());
        if (comment.getRating() < 0 || comment.getRating() > 10) {
            throw new RuntimeException();
        }
    }

    public static void validateImageObject(Image image) {
        validateObjectValue(image);
        validateStringValue(image.getFilePath());
    }

    public static void validateMovieObject(Movie movie) {
        validateObjectValue(movie);
        validateStringValue(movie.getTitle());
        validateStringValue(movie.getDescription());
        validateStringValue(movie.getMovieType().toString());
        validateStringValue(movie.getYearProduction().toString());
    }


    public static void validateObjectValue(Object value) {
        if (value == null) {
            throw new RuntimeException(); //todo: as previously
        }
    }
}
