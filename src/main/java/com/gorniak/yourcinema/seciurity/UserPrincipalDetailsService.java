package com.gorniak.yourcinema.seciurity;

import com.gorniak.yourcinema.user.UserAccount;
import com.gorniak.yourcinema.user.UserAccountProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public final class UserPrincipalDetailsService implements UserDetailsService {
    private final UserAccountProcessor userAccountProcessor;

    private final static Logger logger = LoggerFactory.getLogger(UserPrincipalDetailsService.class);

    public UserPrincipalDetailsService(UserAccountProcessor userAccountProcessor) {
        this.userAccountProcessor = userAccountProcessor;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        logger.info("email: " + email);
        UserAccount userAccount = userAccountProcessor.getUserAccount(email);
        logger.info(userAccount.toString());
        return new UserPrincipal(userAccount);
    }
}
