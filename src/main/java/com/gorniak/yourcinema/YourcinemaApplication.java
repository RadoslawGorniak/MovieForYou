package com.gorniak.yourcinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YourcinemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(YourcinemaApplication.class, args);
	}

}
