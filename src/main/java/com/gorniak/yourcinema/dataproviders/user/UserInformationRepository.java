package com.gorniak.yourcinema.dataproviders.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserInformationRepository extends JpaRepository<UserInformationEntity, Long> {
}
