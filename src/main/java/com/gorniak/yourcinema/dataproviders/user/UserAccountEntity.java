package com.gorniak.yourcinema.dataproviders.user;

import com.gorniak.yourcinema.dataproviders.movie.MovieEntity;
import com.gorniak.yourcinema.user.UserRole;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Set;

@Entity
@Table(name = "user_account")
public class UserAccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Email
    @Column(name = "email", nullable = false, unique = true)
    private String email;
    private String password;
    private String token;
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "userAccount")
    private UserInformationEntity userInformation;

    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "subscribers")
    private Set<MovieEntity> movies;

    public UserAccountEntity() {
    }

    public UserAccountEntity(@Email String email, String password, String token, UserRole userRole) {
        this.email = email;
        this.password = password;
        this.token = token;
        this.userRole = userRole;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public UserInformationEntity getUserInformationDao() {
        return userInformation;
    }

    @Override
    public String toString() {
        return "UserAccountDao{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", token='" + token + '\'' +
                ", userRole=" + userRole +
                ", userInformationDao=" + userInformation +
                '}';
    }
}
