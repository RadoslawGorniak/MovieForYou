package com.gorniak.yourcinema.dataproviders.user;

import com.gorniak.yourcinema.error.exception.EntityNotFoundException;
import com.gorniak.yourcinema.user.UserAccount;
import com.gorniak.yourcinema.user.UserInformation;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserAccountProvider {

    private final static Logger logger = LoggerFactory.getLogger(UserAccountProvider.class);

    private final UserAccountRepository userAccountRepository;
    private final ModelMapper modelMapper;

    public UserAccountProvider(UserAccountRepository userAccountRepository, ModelMapper modelMapper) {
        this.userAccountRepository = userAccountRepository;
        this.modelMapper = modelMapper;
    }


    public void createUser(UserAccount userAccount) {
        UserAccountEntity userAccountEntity = modelMapper.map(userAccount, UserAccountEntity.class);
        userAccountRepository.save(userAccountEntity);
    }


    public UserAccount getUserAccount(String email) {
        try {
            UserAccountEntity userAccountEntity = userAccountRepository.findByEmail(email).orElseThrow(() -> new EntityNotFoundException(UserAccount.class, "email", email));
            return modelMapper.map(userAccountEntity, UserAccount.class);
        } catch (EntityNotFoundException e) {
            logger.warn(e.getMessage());
            throw new EntityNotFoundException(UserAccount.class, "email", email);
        }
    }


    public void addUserInformation(UserInformation userInformation) {

    }
}
