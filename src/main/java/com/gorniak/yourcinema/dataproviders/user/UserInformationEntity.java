package com.gorniak.yourcinema.dataproviders.user;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "users_information")
public class UserInformationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String city;
    private String address;
    private String postcode;
    private LocalDate dateOfBirth;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private UserAccountEntity userAccount;

    public UserInformationEntity() {
    }

    public UserInformationEntity(String firstName, String lastName, String city, String address, String postcode, LocalDate dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.address = address;
        this.postcode = postcode;
        this.dateOfBirth = dateOfBirth;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPostcode() {
        return postcode;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public UserAccountEntity getUserAccountDao() {
        return userAccount;
    }

    @Override
    public String toString() {
        return "UserInformationDao{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", postcode='" + postcode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", userAccountDao=" + userAccount +
                '}';
    }
}
