package com.gorniak.yourcinema.dataproviders.user;

import org.modelmapper.ModelMapper;

public class UserInformationProvider {

    private final ModelMapper modelMapper;
    private final UserInformationRepository userInformationRepository;

    public UserInformationProvider(ModelMapper modelMapper, UserInformationRepository userInformationRepository) {
        this.modelMapper = modelMapper;
        this.userInformationRepository = userInformationRepository;
    }
}
