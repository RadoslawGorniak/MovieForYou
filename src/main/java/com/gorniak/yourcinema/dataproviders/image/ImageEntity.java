package com.gorniak.yourcinema.dataproviders.image;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gorniak.yourcinema.dataproviders.movie.MovieEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class ImageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String filePath;
    private String fileUrl;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "image_movie_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private MovieEntity movie;

    public ImageEntity() {
    }

    public ImageEntity(String filePath, String fileUrl, MovieEntity movie) {
        this.filePath = filePath;
        this.fileUrl = fileUrl;
        this.movie = movie;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public MovieEntity getMovie() {
        return movie;
    }
}
