package com.gorniak.yourcinema.dataproviders.image;

import com.gorniak.yourcinema.error.exception.EntityNotFoundException;
import com.gorniak.yourcinema.image.Image;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class ImageProvider{

    private static final Logger logger = LoggerFactory.getLogger(ImageProvider.class);

    private final ImageRepository imageRepository;
    private final ModelMapper modelMapper;

    public ImageProvider(ImageRepository imageRepository, ModelMapper modelMapper) {
        this.imageRepository = imageRepository;
        this.modelMapper = modelMapper;
    }


    public Page<Image> getImagesFromMovie(String movieTitle, Pageable pageable) {
        return imageRepository.findByMovieTitle(movieTitle, pageable)
                .map(image -> modelMapper.map(image, Image.class));
    }


    public Image getImageFromMovie(String movieTitle, long imageId) {
        ImageEntity imageEntity = imageRepository.findByIdAndMovieTitle(imageId, movieTitle)
                .orElseThrow(() -> new EntityNotFoundException(ImageEntity.class, "id", String.valueOf(imageId)));
        return modelMapper.map(imageEntity, Image.class);
    }


    public void saveImage(Image image) {
        ImageEntity imageEntity = modelMapper.map(image, ImageEntity.class);
        imageRepository.save(imageEntity);
    }


    public boolean exist(long imageId) {
        return imageRepository.existsById(imageId);
    }
}
