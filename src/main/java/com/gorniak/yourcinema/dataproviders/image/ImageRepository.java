package com.gorniak.yourcinema.dataproviders.image;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, Long> {
    Page<ImageEntity> findByMovieTitle(String movieTitle, Pageable pageable);
    Optional<ImageEntity> findByIdAndMovieTitle(long id, String movieTitle);
}
