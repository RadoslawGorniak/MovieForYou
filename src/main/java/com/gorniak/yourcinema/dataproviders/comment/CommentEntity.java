package com.gorniak.yourcinema.dataproviders.comment;

import com.gorniak.yourcinema.dataproviders.movie.MovieEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "comments")
public class CommentEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String author;
    private String message;
    private double rating;
    private LocalDateTime commentDateCreate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "comment_movie_id", nullable = false)
    private MovieEntity movie;

    public CommentEntity() {
    }

    public CommentEntity(String author, String message, double rating, LocalDateTime commentDateCreate, MovieEntity movie) {
        this.author = author;
        this.message = message;
        this.rating = rating;
        this.commentDateCreate = commentDateCreate;
        this.movie = movie;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public double getRating() {
        return rating;
    }

    public LocalDateTime getCommentDateCreate() {
        return commentDateCreate;
    }

    public MovieEntity getMovie() {
        return movie;
    }
}
