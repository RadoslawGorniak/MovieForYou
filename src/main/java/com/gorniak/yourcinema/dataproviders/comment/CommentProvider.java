package com.gorniak.yourcinema.dataproviders.comment;

import com.gorniak.yourcinema.comment.Comment;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class CommentProvider {

    private final CommentRepository commentRepository;
    private final ModelMapper modelMapper;

    public CommentProvider(CommentRepository commentRepository, ModelMapper modelMapper) {
        this.commentRepository = commentRepository;
        this.modelMapper = modelMapper;
    }


    public Page<Comment> getComments(Pageable pageable) {
        return commentRepository.findAll(pageable)
                .map(commentDao -> modelMapper.map(commentDao, Comment.class));
    }
}
