package com.gorniak.yourcinema.dataproviders.comment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, Long> {
    Optional<CommentEntity> findByIdAndMovieId(long commentId, String movieTitle);
    List<CommentEntity> findByMovieTitle(String movieTitle);
}
