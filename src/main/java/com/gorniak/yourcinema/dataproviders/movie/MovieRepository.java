package com.gorniak.yourcinema.dataproviders.movie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends  JpaRepository<MovieEntity, Long> {
    Optional<MovieEntity> findByTitle(String title);
    boolean existsByTitle(String title);
}
