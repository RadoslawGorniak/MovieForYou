package com.gorniak.yourcinema.dataproviders.movie;

import com.gorniak.yourcinema.dataproviders.comment.CommentEntity;
import com.gorniak.yourcinema.dataproviders.user.UserAccountEntity;
import com.gorniak.yourcinema.movie.MovieType;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "movies")
public class MovieEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private MovieType movieType;
    private LocalDate yearProduction;
    private double rating;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "movie")
    private Set<CommentEntity> comments = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "movie_subscribers",
            joinColumns = {@JoinColumn(name = "movie_subscriber_id")},
            inverseJoinColumns = {@JoinColumn(name = "subscriber_movie_id")})
    private Set<UserAccountEntity> subscribers;

    public MovieEntity() {
    }

    public MovieEntity(String title, String description, MovieType movieType, LocalDate yearProduction, double rating) {
        this.title = title;
        this.description = description;
        this.movieType = movieType;
        this.yearProduction = yearProduction;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public LocalDate getYearProduction() {
        return yearProduction;
    }

    public double getRating() {
        return rating;
    }


}
