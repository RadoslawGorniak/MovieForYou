package com.gorniak.yourcinema.dataproviders.movie;

import com.gorniak.yourcinema.error.exception.EntityNotFoundException;
import com.gorniak.yourcinema.movie.Movie;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public class MovieProvider {
    private static final Logger logger = LoggerFactory.getLogger(MovieProvider.class);

    private final MovieRepository movieRepository;
    private final ModelMapper modelMapper;

    public MovieProvider(MovieRepository movieRepository, ModelMapper modelMapper) {
        this.movieRepository = movieRepository;
        this.modelMapper = modelMapper;
    }


    public Page<Movie> getMovies(Pageable pageable) {
        return movieRepository.findAll(pageable)
                .map(movieDao -> modelMapper.map(movieDao, Movie.class));
    }


    public Movie getMovie(String movieTitle) {
        try {
            MovieEntity movieEntity = movieRepository.findByTitle(movieTitle).orElseThrow(() -> new EntityNotFoundException(Movie.class, "title", movieTitle));
            return modelMapper.map(movieEntity, Movie.class);
        } catch (EntityNotFoundException e) {
            logger.warn(e.getMessage());
            throw new EntityNotFoundException(Movie.class, "title", movieTitle);
        }
    }


    public void saveMovie(Movie movie) {
        MovieEntity movieEntity = modelMapper.map(movie, MovieEntity.class);
        movieRepository.save(movieEntity);
    }

    public boolean exist(String movieTitle) {
        return movieRepository.existsByTitle(movieTitle);
    }
}
