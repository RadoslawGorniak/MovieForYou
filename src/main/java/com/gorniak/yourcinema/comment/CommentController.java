package com.gorniak.yourcinema.comment;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;


@RestController
@RequestMapping(value = "/comments", produces = MediaType.APPLICATION_JSON_VALUE)
class CommentController {
    private CommentService commentService;

    CommentController(final CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping()
    ResponseEntity<List<Comment>> getComments(Pageable pageable) {
        List<Comment> comments = commentService.getComments(pageable);
        return new ResponseEntity<>(comments, HttpStatus.FOUND);
    }


}
