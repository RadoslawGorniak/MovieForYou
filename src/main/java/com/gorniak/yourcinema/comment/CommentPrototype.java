package com.gorniak.yourcinema.comment;

public class CommentPrototype {

    private String message;
    private double rating;

    public CommentPrototype( String message, double rating) {
        this.message = message;
        this.rating = rating;
    }

    public String getMessage() {
        return message;
    }

    public double getRating() {
        return rating;
    }
}
