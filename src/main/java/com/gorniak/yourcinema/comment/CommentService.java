package com.gorniak.yourcinema.comment;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    private CommentProcessor commentProcessor;

    public CommentService(CommentProcessor commentProcessor) {
        this.commentProcessor = commentProcessor;
    }

    public List<Comment> getComments(Pageable pageable) {
        return commentProcessor.getComments(pageable).getContent();
    }
}
