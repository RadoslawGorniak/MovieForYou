package com.gorniak.yourcinema.comment;

import com.gorniak.yourcinema.dataproviders.comment.CommentProvider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class CommentProcessor {

    private final CommentProvider commentProvider;

    public CommentProcessor(CommentProvider commentProvider) {
        this.commentProvider = commentProvider;
    }

    public Page<Comment> getComments(Pageable pageable) {
        return commentProvider.getComments(pageable);
    }
}
