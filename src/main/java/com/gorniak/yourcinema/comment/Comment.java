package com.gorniak.yourcinema.comment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gorniak.yourcinema.movie.Movie;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Comment {

    private Long id;
    private String author;
    private String message;
    private double rating;
    private LocalDateTime commentDateCreate;
    @JsonIgnore
    private Movie movie;

    Comment() {
    }

    public Comment(CommentPrototype commentPrototype, String emailUserLogged) {
        this.author = emailUserLogged;
        this.message = commentPrototype.getMessage();
        this.rating = commentPrototype.getRating();
        this.commentDateCreate = LocalDateTime.now();
    }

    public Comment(Long id, CommentPrototype commentPrototype, String emailUserLogged) {
        this(commentPrototype, emailUserLogged);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public double getRating() {
        return rating;
    }

    public Movie getMovie() {
        return movie;
    }

    public LocalDateTime getCommentDateCreate() {
        return commentDateCreate;
    }

    public void addMovie(Movie movie){
        this.movie = movie;
    }
}
