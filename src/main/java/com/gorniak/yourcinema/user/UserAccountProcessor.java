package com.gorniak.yourcinema.user;

import com.gorniak.yourcinema.dataproviders.user.UserAccountProvider;
import com.gorniak.yourcinema.error.exception.UserAccountMissingException;
import com.gorniak.yourcinema.movie.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

public class UserAccountProcessor {
    private final static Logger logger = LoggerFactory.getLogger(UserAccountProcessor.class);

    private PasswordEncoder passwordEncoder;
    private UserAccountProvider userAccountProvider;

    public UserAccountProcessor(PasswordEncoder passwordEncoder, UserAccountProvider userAccountProvider) {
        this.passwordEncoder = passwordEncoder;
        this.userAccountProvider = userAccountProvider;
    }

    public void registerUser(UserAccountPrototype userAccountPrototype) {
        String passwordCode = passwordEncoder.encode(userAccountPrototype.getPassword());
        UserAccount userAccount = new UserAccount(userAccountPrototype.getEmail(), passwordCode, UserRole.USER);
        userAccount.createToken();
        saveUser(userAccount);
    }

    public void addUserInformation(String email, String emailUserLogged, UserInformation userInformation) {
        compareEmailToEmailUserLogged(email, emailUserLogged);
        UserAccount userAccount = getUserAccount(emailUserLogged);
        userAccount.addUserInformation(userInformation);
        saveUser(userAccount);
    }

    public UserAccount getUserAccount(String email) {
        return userAccountProvider.getUserAccount(email);
    }

    public List<Movie> subscribeMovies(String email, String emailUserLogged) {
        compareEmailToEmailUserLogged(email, emailUserLogged);
        UserAccount userAccount = getUserAccount(emailUserLogged);
        return new ArrayList<>(userAccount.getMovies());
    }

    private void compareEmailToEmailUserLogged(String email, String emailUserLogged) {
        try {
            if (!email.equals(emailUserLogged)) {
                throw new UserAccountMissingException(email, emailUserLogged);
            }
        } catch (IllegalArgumentException e) {
            logger.warn(e.getMessage());
            throw new UserAccountMissingException(email, emailUserLogged);
        }
    }

    private void saveUser(UserAccount userAccount) {
        userAccountProvider.createUser(userAccount);
    }
}
