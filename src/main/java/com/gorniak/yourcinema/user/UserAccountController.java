package com.gorniak.yourcinema.user;

import com.gorniak.yourcinema.movie.Movie;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserAccountController {

    private final UserAccountService userAccountService;

    public UserAccountController(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @PostMapping(value = "/registration")
    public ResponseEntity<?> registerUser(@RequestBody UserAccountPrototype userAccountPrototype) {
        userAccountService.registerUser(userAccountPrototype);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(value = "/{email}")
    public ResponseEntity<?> addUserInformation(@PathVariable String email, Principal principal, @RequestBody UserInformation userInformation) {
        userAccountService.addUserInformation(email, principal.getName(), userInformation);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping(value = "/{email}/movies")
    public ResponseEntity<List<Movie>> subscribeMovies(@PathVariable String email, Principal principal){
        List<Movie> movies = userAccountService.subscribeMovies(email, principal.getName());
        return new ResponseEntity<>(movies, HttpStatus.FOUND);
    }

}
