package com.gorniak.yourcinema.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gorniak.yourcinema.movie.Movie;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class UserInformation {
    private Long id;
    private String firstName;
    private String lastName;
    private String city;
    private String address;
    private String postcode;
    private LocalDate dateOfBirth;
    @JsonIgnore
    private UserAccount userAccount;

    private Set<Movie> movies = new HashSet<>();

    public UserInformation() {
    }

    public UserInformation(String firstName, String lastName, String city, String address, String postcode, LocalDate dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.address = address;
        this.postcode = postcode;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPostcode() {
        return postcode;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void addUser(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public String toString() {
        return "UserInformation{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", postcode='" + postcode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", userAccount=" + userAccount +
                '}';
    }
}
