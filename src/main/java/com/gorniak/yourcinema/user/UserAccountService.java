package com.gorniak.yourcinema.user;

import com.gorniak.yourcinema.movie.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAccountService {

    private final UserAccountProcessor userAccountProcessor;

    public UserAccountService(UserAccountProcessor userAccountProcessor) {
        this.userAccountProcessor = userAccountProcessor;
    }

    public void registerUser(UserAccountPrototype userAccountPrototype) {
        userAccountProcessor.registerUser(userAccountPrototype);
    }

    public void addUserInformation(String email, String emailUserLogged, UserInformation userInformation) {
        userAccountProcessor.addUserInformation(email, emailUserLogged, userInformation);
    }

    public List<Movie> subscribeMovies(String email, String emailUserLogged) {

        return userAccountProcessor.subscribeMovies(email, emailUserLogged);

    }
}
