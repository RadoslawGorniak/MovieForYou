package com.gorniak.yourcinema.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gorniak.yourcinema.movie.Movie;
import org.springframework.security.core.userdetails.User;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class UserAccount {

    private Long id;
    private String email;
    private String password;
    private UserRole userRole;
    private String token;
    @JsonIgnore
    private UserInformation userInformation;
    @JsonIgnore
    private Set<Movie> movies = new HashSet<>();

    public UserAccount() {
    }

    public UserAccount(String email, String password, UserRole userRole) {
        this.email = email;
        this.password = password;
        this.userRole = userRole;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public String getToken() {
        return token;
    }

    public void createToken() {
        this.token = UUID.randomUUID().toString();
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void addUserInformation(UserInformation userInformation) {
        userInformation.addUser(this);
        this.userInformation = userInformation;
    }

    public void addToFavoriteMovies(Movie movie) {
        this.movies.add(movie);
    }

    public void removeFromFavoriteMovies(Movie movie) {
        this.movies.remove(movie);
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userRole=" + userRole +
                ", token='" + token + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}

