package com.gorniak.yourcinema.user;

public class UserAccountPrototype {

    private String email;
    private String password;

    public UserAccountPrototype() {
    }

    public UserAccountPrototype(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
