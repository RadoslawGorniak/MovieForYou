package com.gorniak.yourcinema.image;

import com.gorniak.yourcinema.dataproviders.image.ImageProvider;
import com.gorniak.yourcinema.dataproviders.movie.MovieProvider;
import com.gorniak.yourcinema.error.exception.EntityNotFoundException;
import com.gorniak.yourcinema.movie.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public class ImageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(ImageProcessor.class);

    private final ImageProvider imageProvider;
    private final MovieProvider movieProvider;
    private final ImageUploader imageUploader;

    public ImageProcessor(ImageProvider imageProvider, MovieProvider movieProvider, ImageUploader imageUploader) {
        this.imageProvider = imageProvider;
        this.movieProvider = movieProvider;
        this.imageUploader = imageUploader;
    }

    public List<Image> getImagesFromMovie(String movieTitle, Pageable pageable) {
        checkMovieTitleExist(movieTitle);
        return imageProvider.getImagesFromMovie(movieTitle, pageable).getContent();
    }


    public Image getImageFromMovie(long imageId, String movieTitle) {
        checkMovieTitleExist(movieTitle);
        return imageProvider.getImageFromMovie(movieTitle, imageId);
    }

    @Transactional
    public void addImage(String movieTitle, String filePath){
        Movie movie = movieProvider.getMovie(movieTitle);
        Image image = new Image(filePath, creteFileUrl(filePath), movie);
        imageProvider.saveImage(image);
    }

    private String creteFileUrl(String filePath) {
        return imageUploader.upload(filePath);
    }

    private void checkMovieTitleExist(String movieTitle) {
        try {
            if (!movieProvider.exist(movieTitle))
                throw new EntityNotFoundException(Movie.class, "title", movieTitle);
        } catch (EntityNotFoundException e) {
            logger.warn(e.getMessage());
            throw new EntityNotFoundException(Movie.class, "title", movieTitle);
        }
    }
}
