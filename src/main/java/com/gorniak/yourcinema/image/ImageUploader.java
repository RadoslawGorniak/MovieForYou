package com.gorniak.yourcinema.image;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ImageUploader {

    private final Cloudinary cloudinary;

    public ImageUploader(Cloudinary cloudinary) {
        this.cloudinary = cloudinary;
    }

    public String upload(String path) {
        File fileUpload = new File(path);
        Map uploadResult;
        try {
            uploadResult = cloudinary.uploader().upload(fileUpload, ObjectUtils.emptyMap());
        } catch (IOException e) {
            throw new IllegalStateException();
        }
        return uploadResult.get("url").toString();
    }
}
