package com.gorniak.yourcinema.image;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gorniak.yourcinema.movie.Movie;

import javax.persistence.Entity;
import javax.persistence.Table;

public class Image {

    @JsonIgnore
    private String filePath;
    private String fileUrl;

    @JsonIgnore
    private Movie movie;

    public Image() {
    }

    public Image(String filePath) {
        this.filePath = filePath;
    }

    public Image(String filePath, String fileUrl) {
        this.filePath = filePath;
        this.fileUrl = fileUrl;
    }

    public Image(String filePath, String fileUrl, Movie movie) {
        this.filePath = filePath;
        this.fileUrl = fileUrl;
        this.movie = movie;
    }


    public String getFilePath() {
        return filePath;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public Movie getMovie() {
        return movie;
    }

}
