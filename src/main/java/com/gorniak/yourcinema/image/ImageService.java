package com.gorniak.yourcinema.image;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.gorniak.yourcinema.tools.InputValidator.validateNumberValue;
import static com.gorniak.yourcinema.tools.InputValidator.validateStringValue;

@Service
public class ImageService {

    private final ImageProcessor imageProcessor;

    public ImageService(ImageProcessor imageProcessor) {
        this.imageProcessor = imageProcessor;
    }

    public List<Image> getImagesFromMovie(String movieTitle, Pageable pageable) {
        validateStringValue(movieTitle);
        return imageProcessor.getImagesFromMovie(movieTitle, pageable);
    }

    public Image getImageFromMovie(long imageId, String movieTitle) {
        validateStringValue(movieTitle);
        validateNumberValue(imageId);
        return imageProcessor.getImageFromMovie(imageId, movieTitle);
    }

    public void saveImage(String movieTitle, String filePath) {
        validateStringValue(movieTitle);
        validateStringValue(filePath);
        imageProcessor.addImage(movieTitle, filePath);
    }
}
