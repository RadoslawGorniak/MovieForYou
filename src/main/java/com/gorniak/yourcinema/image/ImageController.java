package com.gorniak.yourcinema.image;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/movies/{movieTitle}/images", produces = MediaType.APPLICATION_JSON_VALUE)
public class ImageController {

    private final ImageService imageService;

    public ImageController(final ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping()
    public ResponseEntity<List<Image>> getImagesFromMovie(@PathVariable String movieTitle, Pageable pageable) {
        List<Image> images = imageService.getImagesFromMovie(movieTitle, pageable);
        return new ResponseEntity<>(images, HttpStatus.FOUND);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Image> getImageFromMovie(@PathVariable String movieTitle, @PathVariable Long id) {
        Image image = imageService.getImageFromMovie(id, movieTitle);
        return new ResponseEntity<>(image, HttpStatus.FOUND);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping()
    public ResponseEntity<Image> createImage(@PathVariable String movieTitle, @RequestParam String filePath) {
        imageService.saveImage(movieTitle, filePath);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
