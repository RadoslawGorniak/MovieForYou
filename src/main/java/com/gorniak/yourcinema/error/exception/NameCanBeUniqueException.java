package com.gorniak.yourcinema.error.exception;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class NameCanBeUniqueException extends RuntimeException {

    public NameCanBeUniqueException(Class clazz, String... params){
        super(NameCanBeUniqueException.generateMessage(clazz.getSimpleName(), EntityNotFoundException.toMap(String.class, String.class, params)));
    }

    private static String generateMessage(String entity, Map<String, String> searchParams){
        return StringUtils.capitalize(entity) +
                " have unique parameters " +
                searchParams;
    }
}
