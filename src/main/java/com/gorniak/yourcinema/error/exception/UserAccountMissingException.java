package com.gorniak.yourcinema.error.exception;

public class UserAccountMissingException extends RuntimeException {

    public UserAccountMissingException(String email){
        super(String.format("UserAccount with email: %s already exist", email));
    }

    public UserAccountMissingException(String email, String loggedUserEmail){
        super(String.format("%s you cant modify %s",loggedUserEmail, email));
    }
}
