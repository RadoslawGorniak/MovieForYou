package com.gorniak.yourcinema.configuration;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.gorniak.yourcinema.image.ImageUploader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CloudinaryConfig {
    @Value("${cloudinary.cloudNameValue}")
    private String cloudNameValue;
    @Value("${cloudinary.apiKeyValue}")
    private String apiKeyValue;
    @Value("${cloudinary.apiSecretValue}")
    private String apiSecretValue;

    @Bean
    public Cloudinary cloudinary(){
        return new Cloudinary(ObjectUtils.asMap("cloud_name", cloudNameValue, "api_key", apiKeyValue, "api_secret", apiSecretValue));
    }

    @Bean
    public ImageUploader imageUtils(Cloudinary cloudinary){
        return new ImageUploader(cloudinary);
    }
}
