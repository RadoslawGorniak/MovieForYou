package com.gorniak.yourcinema.configuration;

import com.gorniak.yourcinema.comment.CommentProcessor;
import com.gorniak.yourcinema.dataproviders.comment.CommentProvider;
import com.gorniak.yourcinema.dataproviders.image.ImageProvider;
import com.gorniak.yourcinema.dataproviders.movie.MovieProvider;
import com.gorniak.yourcinema.dataproviders.user.UserAccountProvider;
import com.gorniak.yourcinema.image.ImageProcessor;
import com.gorniak.yourcinema.image.ImageUploader;
import com.gorniak.yourcinema.movie.MovieProcessor;
import com.gorniak.yourcinema.user.UserAccountProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class ProcessorConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public MovieProcessor movieProcessor(MovieProvider movieProvider, UserAccountProvider userAccountProvider) {
        return new MovieProcessor(movieProvider, userAccountProvider);
    }

    @Bean
    public ImageProcessor imageProcessor(ImageProvider imageProvider, MovieProvider movieProvider, ImageUploader imageUploader) {
        return new ImageProcessor(imageProvider, movieProvider, imageUploader);
    }

    @Bean
    public CommentProcessor commentProcessor(CommentProvider commentProvider) {
        return new CommentProcessor(commentProvider);
    }

    @Bean
    public UserAccountProcessor userAccountProcessor(PasswordEncoder passwordEncoder, UserAccountProvider userAccountProvider) {
        return new UserAccountProcessor(passwordEncoder, userAccountProvider);
    }

}
