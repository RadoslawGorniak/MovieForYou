package com.gorniak.yourcinema.configuration;

import com.gorniak.yourcinema.dataproviders.comment.CommentProvider;
import com.gorniak.yourcinema.dataproviders.comment.CommentRepository;
import com.gorniak.yourcinema.dataproviders.image.ImageProvider;
import com.gorniak.yourcinema.dataproviders.image.ImageRepository;
import com.gorniak.yourcinema.dataproviders.movie.MovieProvider;
import com.gorniak.yourcinema.dataproviders.movie.MovieRepository;
import com.gorniak.yourcinema.dataproviders.user.*;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProviderConfig {

    @Bean
    public MovieProvider movieProvider(MovieRepository movieRepository, ModelMapper modelMapper){
        return new MovieProvider(movieRepository, modelMapper);
    }

    @Bean
    public ImageProvider imageProvider(ImageRepository imageRepository, ModelMapper modelMapper){
        return new ImageProvider(imageRepository, modelMapper);
    }

    @Bean
    public CommentProvider commentProvider(CommentRepository commentRepository, ModelMapper modelMapper){
        return new CommentProvider(commentRepository, modelMapper);
    }

    @Bean
    public UserAccountProvider userAccountProvider(UserAccountRepository userAccountRepository, ModelMapper modelMapper){
        return new UserAccountProvider(userAccountRepository, modelMapper);
    }

    @Bean
    public UserInformationProvider userInformationProvider(UserInformationRepository userInformationRepository, ModelMapper modelMapper){
        return new UserInformationProvider(modelMapper, userInformationRepository);
    }
}
