package com.gorniak.yourcinema.movie;

import com.gorniak.yourcinema.comment.Comment;
import com.gorniak.yourcinema.comment.CommentPrototype;
import com.gorniak.yourcinema.dataproviders.movie.MovieProvider;
import com.gorniak.yourcinema.dataproviders.user.UserAccountProvider;
import com.gorniak.yourcinema.error.exception.NameCanBeUniqueException;
import com.gorniak.yourcinema.user.UserAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class MovieProcessor {

    public static final String CANT_CREATE_MOVIE_WITH_THE_SAME_TITLE = "Movie with title: %s already exist, we cant create Movie with the same title";
    private static final Logger logger = LoggerFactory.getLogger(MovieProcessor.class);

    private final MovieProvider movieProvider;
    private final UserAccountProvider userAccountProvider;

    public MovieProcessor(MovieProvider movieProvider, UserAccountProvider userAccountProvider) {
        this.movieProvider = movieProvider;
        this.userAccountProvider = userAccountProvider;
    }

    public List<Movie> getMovies(Pageable pageable) {
        return movieProvider.getMovies(pageable).getContent();
    }

    public Movie getMovie(String movieTitle) {
        return movieProvider.getMovie(movieTitle);
    }

    public void createMovie(Movie movie) {
        String movieTitle = movie.getTitle();
        if (movieProvider.exist(movieTitle)) {
            logger.warn(String.format(CANT_CREATE_MOVIE_WITH_THE_SAME_TITLE, movieTitle));
            throw new NameCanBeUniqueException(Movie.class, "title", movieTitle);
        }
        saveMovie(movie);
    }

    public void addComment(String movieTitle, CommentPrototype commentPrototype, String emailUserLogged) {
        Movie movie = getMovie(movieTitle);
        Comment comment = createComment(commentPrototype, emailUserLogged);
        movie.addComment(comment);
        saveMovie(movie);
    }

    private Comment createComment(CommentPrototype commentPrototype, String emailUserLogged) {
        return new Comment(commentPrototype, emailUserLogged);
    }

    public Comment getCommentFromMovie(String movieTitle, long commentId) {
        Movie movie = getMovie(movieTitle);
        return movie.getComment(commentId);
    }

    public Boolean subscribeMovie(String movieTitle, String emailUserLogged) {
        Movie movie = getMovie(movieTitle);
        UserAccount userAccount = getUserLogged(emailUserLogged);
        boolean isSubscribe = movie.subscribe(userAccount);
        if (isSubscribe)
            saveMovie(movie);
        return isSubscribe;
    }


    public Boolean unsubscribeMovie(String movieTitle, String emailUserLogged){
        Movie movie = getMovie(movieTitle);
        UserAccount userAccount = getUserLogged(emailUserLogged);
        boolean isUnsubscribe = movie.unsubscribe(userAccount);
        if (isUnsubscribe) {
            saveMovie(movie);
        }
        return isUnsubscribe;
    }

    private UserAccount getUserLogged(String emailUserLogged) {
        return userAccountProvider.getUserAccount(emailUserLogged);
    }

    private void saveMovie(Movie movie) {
        movieProvider.saveMovie(movie);
    }
}
