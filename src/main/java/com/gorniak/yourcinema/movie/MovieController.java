package com.gorniak.yourcinema.movie;

import com.gorniak.yourcinema.comment.Comment;
import com.gorniak.yourcinema.comment.CommentPrototype;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(path = "/movies", produces = MediaType.APPLICATION_JSON_VALUE)
class MovieController {

    private final MovieService movieService;

    MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping()
    ResponseEntity<List<Movie>> getMovies(Pageable pageable) {
        List<Movie> movies = movieService.getMovies(pageable);
        return ResponseEntity.status(HttpStatus.FOUND).body(movies);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping()
    ResponseEntity<Movie> createMovie(@RequestBody Movie movie) {
        movieService.createMovie(movie);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(value = "/{movieTitle}")
    ResponseEntity<Movie> getMovie(@PathVariable String movieTitle) {
        Movie movie = movieService.getMovie(movieTitle);
        return ResponseEntity.status(HttpStatus.FOUND).body(movie);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(value = "/{movieTitle}/comments")
    ResponseEntity<?> addComment(@PathVariable String movieTitle, @RequestBody CommentPrototype commentPrototype, Principal principal) {
        movieService.addComment(movieTitle, commentPrototype, principal.getName());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/{movieTitle}/comments/{commentId}")
    ResponseEntity<Comment> getCommentFromMovie(@PathVariable String movieTitle, @PathVariable long commentId) {
        Comment commentFromMovie = movieService.getCommentFromMovie(movieTitle, commentId);
        return new ResponseEntity<>(commentFromMovie, HttpStatus.FOUND);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(value = "/{movieTitle}/subscribe")
    ResponseEntity<Boolean> subscribeMovie(@PathVariable String movieTitle, Principal principal){
        return new ResponseEntity<>(movieService.subscribeMovie(movieTitle, principal.getName()), HttpStatus.OK);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping(value = "/{movieTitle}/unsubscribe")
    ResponseEntity<Boolean> unsubscribe(@PathVariable String movieTitle, Principal principal){
        return new ResponseEntity<>(movieService.unsubscribeMovie(movieTitle, principal.getName()), HttpStatus.OK);
    }

}
