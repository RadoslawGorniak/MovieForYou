package com.gorniak.yourcinema.movie;

import com.gorniak.yourcinema.comment.Comment;
import com.gorniak.yourcinema.comment.CommentPrototype;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.gorniak.yourcinema.tools.InputValidator.*;

@Service
public class MovieService {

    private final MovieProcessor movieProcessor;

    public MovieService(MovieProcessor movieProcessor) {
        this.movieProcessor = movieProcessor;
    }

    public List<Movie> getMovies(Pageable pageable) {
        return movieProcessor.getMovies(pageable);
    }

    public Movie getMovie(String movieTitle) {
        validateStringValue(movieTitle);
        return movieProcessor.getMovie(movieTitle);
    }

    public void createMovie(Movie movie) {
        validateMovieObject(movie);
        movieProcessor.createMovie(movie);
    }

    public void addComment(String movieTitle, CommentPrototype commentPrototype, String emailUserLogged) {
        validateStringValue(movieTitle);
        validateObjectValue(commentPrototype);//Todo Added validate for commentPrototype
        validateStringValue(emailUserLogged);
        movieProcessor.addComment(movieTitle, commentPrototype, emailUserLogged);
    }

    public Comment getCommentFromMovie(String movieTitle, long commentId) {
        validateStringValue(movieTitle);
        validateNumberValue(commentId);
        return movieProcessor.getCommentFromMovie(movieTitle, commentId);
    }

    public Boolean subscribeMovie(String movieTitle, String emailUserLogged) {
        validateStringValue(movieTitle);
        validateStringValue(emailUserLogged);
        return movieProcessor.subscribeMovie(movieTitle, emailUserLogged);
    }

    public Boolean unsubscribeMovie(String movieTitle, String emailUserLogged) {
        validateStringValue(movieTitle);
        validateStringValue(emailUserLogged);
        return movieProcessor.unsubscribeMovie(movieTitle, emailUserLogged);
    }
}
