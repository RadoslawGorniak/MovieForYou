package com.gorniak.yourcinema.movie;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gorniak.yourcinema.comment.Comment;
import com.gorniak.yourcinema.user.UserAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class Movie {

    private final static Logger logger = LoggerFactory.getLogger(Movie.class);
    public static final String COMMENT_NOT_EXIST_IN_COMMENTS_LIST_FROM_MOVIE = "Comment not exist in comments list from Movie";


    private Long id;
    private String title;
    private String description;
    private MovieType movieType;
    private LocalDate yearProduction;
    private double rating;
    @JsonIgnore
    private Set<Comment> comments = new HashSet<>();
    @JsonIgnore
    private Set<UserAccount> subscribers = new HashSet<>();

    public Movie() {
    }

    public Movie(String title, String description, MovieType movieType, LocalDate yearProduction, double rating) {
        this.title = title;
        this.description = description;
        this.movieType = movieType;
        this.yearProduction = yearProduction;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public LocalDate getYearProduction() {
        return yearProduction;
    }

    public double getRating() {
        return rating;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Set<UserAccount> getSubscribers() {
        return subscribers;
    }

    public void addComment(Comment comment) {
        comment.addMovie(this);
        this.comments.add(comment);
        recalculateRating();
    }

    public int countComments() {
        return this.comments.size();
    }

    private void recalculateRating() {
        Double ratingSum = this.comments.stream()
                .map(Comment::getRating)
                .reduce(0.0, Double::sum);
        this.rating = ratingSum / countComments();
    }

    public Comment getComment(long commentId) {
        try {
            return this.comments.stream()
                    .filter(comment -> comment.getId().equals(commentId))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException(COMMENT_NOT_EXIST_IN_COMMENTS_LIST_FROM_MOVIE));
        } catch (IllegalArgumentException e) {
            logger.warn(e.getMessage());
            throw new IllegalArgumentException(COMMENT_NOT_EXIST_IN_COMMENTS_LIST_FROM_MOVIE);
        }
    }

    public boolean subscribe(UserAccount userAccount) {
        if (subscribers.contains(userAccount))
            return false;
        userAccount.addToFavoriteMovies(this);
        return this.subscribers.add(userAccount);
    }

    public boolean unsubscribe(UserAccount userAccount) {
        userAccount.removeFromFavoriteMovies(this);
        return this.subscribers.remove(userAccount);
    }

    public int quantitySubscribe(){
        return this.subscribers.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(title, movie.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
