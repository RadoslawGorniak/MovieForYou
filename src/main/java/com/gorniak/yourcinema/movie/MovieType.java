package com.gorniak.yourcinema.movie;

public enum MovieType {
    FANTASY, HORROR, ROMANTIC
}
