package com.gorniak.yourcinema.comment;

import com.gorniak.yourcinema.dataproviders.comment.CommentProvider;
import com.gorniak.yourcinema.dataproviders.movie.MovieProvider;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class CommentProcessorTest {

    static final String TEST_TITLE = "TestTitle";
    static final String TEST_DESCRIPTION = "TestDescription";
    static final String TEST_AUTHOR = "TestAuthor";
    static final String TEST_MESSAGE = "TestMessage";


    @Mock
    CommentProvider commentProvider;

    @Mock
    MovieProvider movieProvider;

    @InjectMocks
    CommentProcessor commentProcessor;


//    @Test
//    void getCommentsFromMovieShouldReturnCommentListWhenMovieWithTitleExist(){
//        //given
//        when(movieProvider.exist(TEST_TITLE)).thenReturn(true);
//        when(commentProvider.getCommentsFromMovie(TEST_TITLE)).thenReturn()//TODO stworzyc liste
//    }

}
