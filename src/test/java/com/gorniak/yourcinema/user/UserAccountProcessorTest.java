package com.gorniak.yourcinema.user;

import com.gorniak.yourcinema.dataproviders.user.UserAccountProvider;
import com.gorniak.yourcinema.error.exception.UserAccountMissingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class UserAccountProcessorTest {

    static final String TEST_EMAIL = "test@test.pl";
    static final String TEST_EMAIL_USER_LOGGED = "test@test.pl";
    static final String TEST_PASSWORD = "test123";
    static final String TEST_FIRST_NAME = "testFirstName";
    static final String TEST_LAST_NAME = "testLastName";
    static final String TEST_CITY = "testCity";
    static final String TEST_ADDRESS = "testAddress";
    static final String TEST_POSTCODE = "testPostcode";
    public static final String TEST_DIFFERENT_EMAIL = "testDifferentEmail";
    public static final String EXCEPTION_CONTAINING_MESSAGE = "you cant modify";

    @Mock
    UserAccountProvider userAccountProvider;


    UserAccountProcessor userAccountProcessor;
    UserAccount userAccount;
    UserInformation userInformation;

    @BeforeEach
    void setUp() {

        userAccountProcessor = new UserAccountProcessor(passwordEncoder(), userAccountProvider);
        userAccount = new UserAccount(TEST_EMAIL, TEST_PASSWORD, UserRole.USER);
        userInformation = new UserInformation(TEST_FIRST_NAME, TEST_LAST_NAME, TEST_CITY, TEST_ADDRESS, TEST_POSTCODE, LocalDate.now());
    }


    @Test
    void addUserShouldSaveUser() {
        //given
        UserAccountPrototype userAccountPrototype = new UserAccountPrototype(TEST_EMAIL, TEST_PASSWORD);

        //when
        userAccountProcessor.registerUser(userAccountPrototype);

        //then
        verify(userAccountProvider, times(1)).createUser(any(UserAccount.class));
    }

    @Test
    void addUserInformationShouldSaveUserInformationInUserAccount() {
        //given
        when(userAccountProvider.getUserAccount(TEST_EMAIL)).thenReturn(userAccount);

        //when
        userAccountProcessor.addUserInformation(TEST_EMAIL, TEST_EMAIL_USER_LOGGED, userInformation);

        //then
        verify(userAccountProvider, times(1)).createUser(userAccount);
    }

    @Test
    void addUserInformationShouldThrowExceptionWhenEmailIsNotEqualsEmailLogged() {
        //when
        assertThatExceptionOfType(UserAccountMissingException.class)
                .isThrownBy(() -> userAccountProcessor.addUserInformation(TEST_DIFFERENT_EMAIL, TEST_EMAIL_USER_LOGGED, userInformation)).withMessageContaining(EXCEPTION_CONTAINING_MESSAGE);
        verify(userAccountProvider, times(0)).getUserAccount(TEST_EMAIL);
    }


    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
