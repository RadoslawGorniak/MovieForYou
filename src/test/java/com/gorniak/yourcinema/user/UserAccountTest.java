package com.gorniak.yourcinema.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class UserAccountTest {

    static final String TEST_EMAIL = "test@test.pl";
    static final String TEST_PASSWORD = "test123";
    static final String TEST_FIRST_NAME = "testFirstName";
    static final String TEST_LAST_NAME = "testLastName";
    static final String TEST_CITY = "testCity";
    static final String TEST_ADDRESS = "testAddress";
    static final String TEST_POSTCODE = "testPostcode";


    @Test
    void addUserInformationShouldInitialUserInformation() {
        //given
        UserAccount userAccount = new UserAccount(TEST_EMAIL, TEST_PASSWORD, UserRole.USER);
        UserInformation userInformation = new UserInformation(TEST_FIRST_NAME, TEST_LAST_NAME, TEST_CITY, TEST_ADDRESS, TEST_POSTCODE, LocalDate.now());

        //when
        userAccount.addUserInformation(userInformation);

        //then
        assertThat(userAccount.getUserInformation()).isEqualTo(userInformation);
        assertThat(userInformation.getUserAccount()).isEqualTo(userAccount);
    }

}
