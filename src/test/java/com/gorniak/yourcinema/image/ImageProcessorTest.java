package com.gorniak.yourcinema.image;

import com.gorniak.yourcinema.dataproviders.image.ImageProvider;
import com.gorniak.yourcinema.dataproviders.movie.MovieProvider;
import com.gorniak.yourcinema.error.exception.EntityNotFoundException;
import com.gorniak.yourcinema.movie.Movie;
import com.gorniak.yourcinema.movie.MovieType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class ImageProcessorTest {

    static final String TEST_TITLE = "TestTitle";
    static final String TEST_DESCRIPTION = "TestDescription";
    static final String TEST_FILE_PATH = "TestFilePath";
    static final String TEST_FILE_URL = "TestFileUrl";
    static final String TEST_FILE_PATH_2 = "TestFilePath2";
    static final String TEST_FILE_URL_2 = "TestFileUrl2";

    @Mock
    ImageProvider imageProvider;

    @Mock
    MovieProvider movieProvider;

    @Mock
    ImageUploader imageUploader;

    @InjectMocks
    ImageProcessor imageProcessor;

    Movie movie;
    Image image;

    @BeforeEach
    void setUp() {
        movie = new Movie(TEST_TITLE, TEST_DESCRIPTION, MovieType.FANTASY, LocalDate.now(), 10.0);
        image = new Image(TEST_FILE_PATH, TEST_FILE_URL, movie);
    }

    @Test
    void getImagesFromMovieShouldReturnListImageWhenMovieExist() {
        //given
        Pageable pageable = PageRequest.of(0, 5);
        when(movieProvider.exist(TEST_TITLE)).thenReturn(true);
        when(imageProvider.getImagesFromMovie(TEST_TITLE, pageable)).thenReturn(createTestPage(pageable));

        //when
        List<Image> imagesFromMovie = imageProcessor.getImagesFromMovie(TEST_TITLE, pageable);

        //then
        assertAll(
                () -> assertThat(imagesFromMovie.size()).isEqualTo(2),
                () -> {
                    Image image = imagesFromMovie.get(0);
                    assertThat(image.getFileUrl()).isEqualTo(TEST_FILE_URL);
                    assertThat(image.getFilePath()).isEqualTo(TEST_FILE_PATH);
                },
                () -> {
                    Image image = imagesFromMovie.get(1);
                    assertThat(image.getFileUrl()).isEqualTo(TEST_FILE_URL_2);
                    assertThat(image.getFilePath()).isEqualTo(TEST_FILE_PATH_2);
                },
                () -> {
                    Movie movie = imagesFromMovie.get(0).getMovie();
                    assertThat(movie.getTitle()).isEqualTo(TEST_TITLE);
                    assertThat(movie.getDescription()).isEqualTo(TEST_DESCRIPTION);
                }
        );
        verify(movieProvider, times(1)).exist(TEST_TITLE);
        verify(imageProvider, times(1)).getImagesFromMovie(TEST_TITLE, pageable);
    }

    @Test
    void getImagesFromMovieShouldThrowsExceptionWhenMovieNoExist() {
        //given
        Pageable pageable = PageRequest.of(0, 5);
        when(movieProvider.exist(TEST_TITLE)).thenReturn(false);

        //when+then
        assertThatExceptionOfType(EntityNotFoundException.class)
                .isThrownBy(() -> imageProcessor.getImagesFromMovie(TEST_TITLE, pageable)).withMessageContaining(TEST_TITLE);
        verify(movieProvider, times(1)).exist(TEST_TITLE);
    }

    @Test
    void getImageFromMovieShouldReturnImageWhenMovieExist() {
        //given
        when(movieProvider.exist(TEST_TITLE)).thenReturn(true);
        when(imageProvider.getImageFromMovie(TEST_TITLE, 1)).thenReturn(image);

        //when
        Image imageFromMovie = imageProcessor.getImageFromMovie(1, TEST_TITLE);

        //then
        assertAll(
                () -> assertThat(imageFromMovie.getFilePath()).isEqualTo(TEST_FILE_PATH),
                () -> assertThat(imageFromMovie.getFileUrl()).isEqualTo(TEST_FILE_URL),
                () -> {
                    Movie movie = imageFromMovie.getMovie();
                    assertThat(movie.getTitle()).isEqualTo(TEST_TITLE);
                    assertThat(movie.getDescription()).isEqualTo(TEST_DESCRIPTION);
                }
        );
        verify(movieProvider, times(1)).exist(TEST_TITLE);
        verify(imageProvider, times(1)).getImageFromMovie(TEST_TITLE, 1);
    }

    @Test
    void getImageFromMovieShouldThrowsExceptionWhenMovieExist() {
        //given
        when(movieProvider.exist(TEST_TITLE)).thenReturn(false);

        //when+then
        assertThatExceptionOfType(EntityNotFoundException.class)
                .isThrownBy(() -> imageProcessor.getImageFromMovie(1, TEST_TITLE)).withMessageContaining(TEST_TITLE);
        verify(movieProvider, times(1)).exist(TEST_TITLE);
    }

    @Test
    void addImageOkTest() {
        //given
        when(movieProvider.getMovie(TEST_TITLE)).thenReturn(movie);
        when(imageUploader.upload(TEST_FILE_PATH)).thenReturn(TEST_FILE_URL);

        //when
        imageProcessor.addImage(TEST_TITLE, TEST_FILE_PATH);

        //then
        verify(movieProvider, times(1)).getMovie(TEST_TITLE);
        verify(imageUploader, times(1)).upload(TEST_FILE_PATH);
        verify(imageProvider, times(1)).saveImage(any(Image.class));
    }

    Page<Image> createTestPage(Pageable pageable) {
        Image image1 = new Image(TEST_FILE_PATH, TEST_FILE_URL, movie);
        Image image2 = new Image(TEST_FILE_PATH_2, TEST_FILE_URL_2, movie);
        List<Image> imageDaoList = Arrays.asList(image1, image2);
        return new PageImpl<>(imageDaoList, pageable, imageDaoList.size());
    }
}
