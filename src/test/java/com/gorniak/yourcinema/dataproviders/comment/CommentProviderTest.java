package com.gorniak.yourcinema.dataproviders.comment;

import com.gorniak.yourcinema.comment.Comment;
import com.gorniak.yourcinema.dataproviders.movie.MovieEntity;
import com.gorniak.yourcinema.movie.MovieType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class CommentProviderTest {

    static final String TEST_TITLE = "TestTitle";
    static final String TEST_DESCRIPTION = "TestDescription";
    static final String TEST_AUTHOR = "TestAuthor";
    static final String TEST_MESSAGE = "TestMessage";
    static final String TEST_AUTHOR_2 = "TestAuthor2";
    static final String TEST_MESSAGE_2 = "TestMessage2";

    @Mock
    CommentRepository commentRepository;

    ModelMapper modelMapper;
    CommentProvider commentProvider;
    MovieEntity movieEntity;
    Pageable pageable;

    @BeforeEach
    void setUp() {
        modelMapper = createModelMapper();
        commentProvider = new CommentProvider(commentRepository, modelMapper);
        movieEntity = new MovieEntity(TEST_TITLE, TEST_DESCRIPTION, MovieType.FANTASY, LocalDate.now(), 10.0);
        pageable = PageRequest.of(0, 5);
    }

    @Test
    void getAllCommentsShouldReturnPageList() {
        //given
        Page<CommentEntity> commentsDaoPage = createTestPage();
        when(commentRepository.findAll(pageable)).thenReturn(commentsDaoPage);

        //when
        Page<Comment> commentsPage = commentProvider.getComments(pageable);

        //then
        assertThat(commentsPage.getTotalPages()).isEqualTo(1);
        assertThat(commentsPage.getTotalElements()).isEqualTo(2);
        assertAll(
                () -> assertThat(commentsPage.getContent().size()).isEqualTo(2),
                () -> {
                    Comment comment = commentsPage.getContent().get(0);
                    assertThat(comment.getRating()).isEqualTo(10.0);
                    assertThat(comment.getAuthor()).isEqualTo(TEST_AUTHOR);
                    assertThat(comment.getMessage()).isEqualTo(TEST_MESSAGE);
                    assertThat(comment.getMovie().getTitle()).isEqualTo(TEST_TITLE);
                    assertThat(comment.getMovie().getDescription()).isEqualTo(TEST_DESCRIPTION);
                },
                () -> {
                    Comment comment = commentsPage.getContent().get(1);
                    assertThat(comment.getRating()).isEqualTo(10.0);
                    assertThat(comment.getAuthor()).isEqualTo(TEST_AUTHOR_2);
                    assertThat(comment.getMessage()).isEqualTo(TEST_MESSAGE_2);
                    assertThat(comment.getMovie().getTitle()).isEqualTo(TEST_TITLE);
                    ;
                    assertThat(comment.getMovie().getDescription()).isEqualTo(TEST_DESCRIPTION);
                }
        );
    }

    Page<CommentEntity> createTestPage() {
        CommentEntity commentEntity1 = new CommentEntity(TEST_AUTHOR, TEST_MESSAGE, 10.0, LocalDateTime.now(), movieEntity);
        CommentEntity commentEntity2 = new CommentEntity(TEST_AUTHOR_2, TEST_MESSAGE_2, 10.0, LocalDateTime.now(), movieEntity);
        List<CommentEntity> commentEntityList = Arrays.asList(commentEntity1, commentEntity2);
        return new PageImpl<>(commentEntityList, pageable, commentEntityList.size());
    }

    ModelMapper createModelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        return mapper;
    }
}
