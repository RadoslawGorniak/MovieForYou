package com.gorniak.yourcinema.dataproviders.user;

import com.gorniak.yourcinema.user.UserAccount;
import com.gorniak.yourcinema.user.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class UserAccountProviderTest {

    static final String TEST_EMAIL = "test@test.pl";
    static final String TEST_PASSWORD = "test123";
    public static final String TEST_TOKEN = "testToken";


    @Mock
    UserAccountRepository userAccountRepository;

    ModelMapper modelMapper;
    UserAccountProvider userAccountProvider;
    UserAccount userAccount;

    @BeforeEach
    void setUp(){
        modelMapper = createModelMapper();
        userAccountProvider = new UserAccountProvider(userAccountRepository, modelMapper);
        userAccount = new UserAccount(TEST_EMAIL, TEST_PASSWORD, UserRole.USER);
    }

    @Test
    void saveUserOkTest(){
        //given+when
        userAccountProvider.createUser(userAccount);

        //then
        verify(userAccountRepository, times(1)).save(any(UserAccountEntity.class));
    }

    @Test
    void getUserAccountShouldReturnUserAccount(){
        //given
        UserAccountEntity userAccountEntity = new UserAccountEntity(TEST_EMAIL, TEST_PASSWORD, TEST_TOKEN, UserRole.USER);
        when(userAccountRepository.findByEmail(TEST_EMAIL)).thenReturn(Optional.of(userAccountEntity));

        //when
        UserAccount userAccount = userAccountProvider.getUserAccount(TEST_EMAIL);

        //then
        assertAll(
                () -> {
                    assertThat(userAccount.getEmail()).isEqualTo(userAccountEntity.getEmail());
                    assertThat(userAccount.getPassword()).isEqualTo(userAccountEntity.getPassword());
                    assertThat(userAccount.getUserRole().toString()).isEqualTo(userAccountEntity.getUserRole().toString());
                    assertThat(userAccount.getToken()).isEqualTo(userAccountEntity.getToken());
                }
        );
    }


    ModelMapper createModelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        return mapper;
    }
}
