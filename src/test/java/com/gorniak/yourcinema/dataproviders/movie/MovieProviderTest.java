package com.gorniak.yourcinema.dataproviders.movie;

import com.gorniak.yourcinema.error.exception.EntityNotFoundException;
import com.gorniak.yourcinema.movie.Movie;
import com.gorniak.yourcinema.movie.MovieType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class MovieProviderTest {

    static final String TEST_TITLE_1 = "TestTitle1";
    static final String TEST_TITLE_2 = "TestTitle2";
    static final String TEST_DESCRIPTION_1 = "TestDescription1";
    static final String TEST_DESCRIPTION_2 = "TestDescription2";

    @Mock
    MovieRepository movieRepository;
    MovieProvider movieProvider;
    ModelMapper modelMapper;

    @BeforeEach
    public void setUp() {
        modelMapper = createModelMapper();
        movieProvider = new MovieProvider(movieRepository, modelMapper);
    }

    @Test
    void getMoviesOkTest() {
        //given
        Pageable pageable = PageRequest.of(0, 5);
        Page<MovieEntity> testPage = createTestPage(pageable);
        when(movieRepository.findAll(pageable)).thenReturn(testPage);

        //when
        Page<Movie> moviesPage = movieProvider.getMovies(pageable);
        List<Movie> movies = moviesPage.getContent();

        //then
        assertAll(
                () -> assertThat(movies.size()).isEqualTo(2),
                () -> assertThat(movies.get(0).getTitle()).isEqualTo(TEST_TITLE_1),
                () -> assertThat(movies.get(0).getDescription()).isEqualTo(TEST_DESCRIPTION_1),
                () -> assertThat(movies.get(0).getYearProduction().getMonth()).isEqualTo(LocalDate.now().getMonth()),
                () -> assertThat(movies.get(0).getMovieType()).isEqualTo(MovieType.FANTASY),
                () -> assertThat(movies.get(1).getTitle()).isEqualTo(TEST_TITLE_2),
                () -> assertThat(movies.get(1).getDescription()).isEqualTo(TEST_DESCRIPTION_2),
                () -> assertThat(movies.get(1).getYearProduction().getMonth()).isEqualTo(LocalDate.now().getMonth()),
                () -> assertThat(movies.get(1).getMovieType()).isEqualTo(MovieType.HORROR)
        );
        verify(movieRepository, times(1)).findAll(pageable);
    }

    @Test
    void getMovieOkTest() {
        //given
        MovieEntity movieEntity = new MovieEntity(TEST_TITLE_1, TEST_DESCRIPTION_1, MovieType.FANTASY, LocalDate.now(), 10.0);
        when(movieRepository.findByTitle(TEST_TITLE_1)).thenReturn(Optional.of(movieEntity));

        //when
        Movie movie = movieProvider.getMovie(TEST_TITLE_1);

        //then
        assertAll(
                () -> assertThat(movie.getTitle()).isEqualTo(TEST_TITLE_1),
                () -> assertThat(movie.getDescription()).isEqualTo(TEST_DESCRIPTION_1),
                () -> assertThat(movie.getYearProduction().getMonth()).isEqualTo(LocalDate.now().getMonth()),
                () -> assertThat(movie.getMovieType()).isEqualTo(MovieType.FANTASY)
        );
        verify(movieRepository, times(1)).findByTitle(TEST_TITLE_1);
    }

    @Test
    void getMovieThrowExceptionWhenMovieNotExist() {
        //given
        when(movieRepository.findByTitle(TEST_TITLE_1)).thenReturn(Optional.empty());

        //when+then
        assertThatExceptionOfType(EntityNotFoundException.class)
                .isThrownBy(() -> movieProvider.getMovie(TEST_TITLE_1)).withMessageContaining(TEST_TITLE_1);

    }

    @Test
    void saveMovieOkTest() {
        //given
        Movie movie = new Movie(TEST_TITLE_1, TEST_DESCRIPTION_1, MovieType.FANTASY, LocalDate.now(), 10.0);

        //when
        movieProvider.saveMovie(movie);

        //then
        verify(movieRepository, times(1)).save(any(MovieEntity.class));
    }

    @Test
    void existWillReturnTrueWhenExistByTitleIsTrue() {
        //given
        when(movieRepository.existsByTitle(TEST_TITLE_1)).thenReturn(true);

        //when
        boolean exist = movieProvider.exist(TEST_TITLE_1);

        //then
        assertThat(exist).isTrue();
        verify(movieRepository, times(1)).existsByTitle(TEST_TITLE_1);
    }

    @Test
    void existWillReturnFalseWhenExistByTitleIsFalse() {
        //given
        when(movieRepository.existsByTitle(TEST_TITLE_1)).thenReturn(false);

        //when
        boolean exist = movieProvider.exist(TEST_TITLE_1);

        //then
        assertThat(exist).isFalse();
        verify(movieRepository, times(1)).existsByTitle(TEST_TITLE_1);
    }

    Page<MovieEntity> createTestPage(Pageable pageable) {
        MovieEntity movieEntity1 = new MovieEntity(TEST_TITLE_1, TEST_DESCRIPTION_1, MovieType.FANTASY, LocalDate.now(), 10.0);
        MovieEntity movieEntity2 = new MovieEntity(TEST_TITLE_2, TEST_DESCRIPTION_2, MovieType.HORROR, LocalDate.now(), 5.0);
        List<MovieEntity> movies = Arrays.asList(movieEntity1, movieEntity2);
        return new PageImpl<>(movies, pageable, movies.size());
    }

    ModelMapper createModelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        return mapper;
    }

}
