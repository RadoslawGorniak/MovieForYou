package com.gorniak.yourcinema.dataproviders.image;

import com.gorniak.yourcinema.dataproviders.movie.MovieEntity;
import com.gorniak.yourcinema.error.exception.EntityNotFoundException;
import com.gorniak.yourcinema.image.Image;
import com.gorniak.yourcinema.movie.Movie;
import com.gorniak.yourcinema.movie.MovieType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class ImageProviderTest {

    static final String TEST_TITLE = "TestTitle";
    static final String TEST_DESCRIPTION = "TestDescription";
    static final String TEST_FILE_PATH_1 = "TestFilePath1";
    static final String TEST_FILE_URL_1 = "TestFileUrl1";
    static final String TEST_FILE_PATH_2 = "TestFilePath2";
    static final String TEST_FILE_URL_2 = "TestFileUrl2";
    static final String CONTAINING_IMAGE_ENTITY = "ImageEntity";

    @Mock
    ImageRepository imageRepository;

    ImageProvider imageProvider;
    ModelMapper modelMapper;
    MovieEntity movieEntity;

    @BeforeEach
    void setUp() {
        modelMapper = createModelMapper();
        imageProvider = new ImageProvider(imageRepository, modelMapper);
        movieEntity = new MovieEntity(TEST_TITLE, TEST_DESCRIPTION, MovieType.FANTASY, LocalDate.now(), 10.0);
    }

    @Test
    void getImagesShouldReturnPage() {
        //given
        Pageable pageable = PageRequest.of(0, 5);
        Page<ImageEntity> ImagesDaoPage = createTestPage(pageable);
        when(imageRepository.findByMovieTitle(TEST_TITLE, pageable)).thenReturn(ImagesDaoPage);

        //when
        Page<Image> imagesPage = imageProvider.getImagesFromMovie(TEST_TITLE, pageable);
        List<Image> images = imagesPage.getContent();

        //then
        assertAll(
                () -> assertThat(imagesPage.getTotalElements()).isEqualTo(2),
                () -> assertThat(imagesPage.getTotalPages()).isEqualTo(1),
                () -> assertThat(images.size()).isEqualTo(2),
                () -> assertThat(images.get(0).getMovie().getTitle()).isEqualTo(TEST_TITLE),
                () -> assertThat(images.get(0).getFilePath()).isEqualTo(TEST_FILE_PATH_1),
                () -> assertThat(images.get(0).getFileUrl()).isEqualTo(TEST_FILE_URL_1),
                () -> assertThat(images.get(1).getMovie().getTitle()).isEqualTo(TEST_TITLE),
                () -> assertThat(images.get(1).getFilePath()).isEqualTo(TEST_FILE_PATH_2),
                () -> assertThat(images.get(1).getFileUrl()).isEqualTo(TEST_FILE_URL_2)
        );
    }

    @Test
    void getImageShouldReturnImage(){
        //given
        ImageEntity imageEntity = new ImageEntity(TEST_FILE_PATH_1, TEST_FILE_URL_1, movieEntity);
        when(imageRepository.findByIdAndMovieTitle(1, TEST_TITLE)).thenReturn(Optional.of(imageEntity));

        //when
        Image image = imageProvider.getImageFromMovie(TEST_TITLE, 1);

        //then
        assertAll(
                () -> assertThat(image.getMovie().getTitle()).isEqualTo(TEST_TITLE),
                () -> assertThat(image.getFilePath()).isEqualTo(TEST_FILE_PATH_1),
                () -> assertThat(image.getFileUrl()).isEqualTo(TEST_FILE_URL_1)
        );
        verify(imageRepository, times(1)).findByIdAndMovieTitle(1, TEST_TITLE);
    }

    @Test
    void getImageShouldThrownExceptionWhenImageNotExist(){
        //given
        when(imageRepository.findByIdAndMovieTitle(1, TEST_TITLE)).thenReturn(Optional.empty());

        //when+then
        assertThatExceptionOfType(EntityNotFoundException.class)
                .isThrownBy(() -> imageProvider.getImageFromMovie(TEST_TITLE, 1)).withMessageContaining(CONTAINING_IMAGE_ENTITY);
    }

    @Test
    void saveImageOkTest(){
        //given
        Movie movie = new Movie(TEST_TITLE, TEST_DESCRIPTION, MovieType.FANTASY, LocalDate.now(), 10.0);
        Image image = new Image(TEST_FILE_PATH_1, TEST_FILE_URL_1, movie);

        //when
        imageProvider.saveImage(image);

        //then
        verify(imageRepository, times(1)).save(any(ImageEntity.class));
    }

    @Test
    void existWillReturnTrueWhenExistByImageIdIsTrue(){
        //given
        when(imageRepository.existsById(anyLong())).thenReturn(true);

        //when
        boolean exist = imageProvider.exist(1);

        //then
        assertThat(exist).isTrue();
        verify(imageRepository, times(1)).existsById(anyLong());
    }

    @Test
    void existWillReturnFalseWhenExistByImageIdIsFalse(){
        //given
        when(imageRepository.existsById(anyLong())).thenReturn(false);

        //when
        boolean exist = imageProvider.exist(1);

        //then
        assertThat(exist).isFalse();
        verify(imageRepository, times(1)).existsById(anyLong());
    }

    Page<ImageEntity> createTestPage(Pageable pageable) {
        ImageEntity imageEntity1 = new ImageEntity(TEST_FILE_PATH_1, TEST_FILE_URL_1, movieEntity);
        ImageEntity imageEntity2 = new ImageEntity(TEST_FILE_PATH_2, TEST_FILE_URL_2, movieEntity);
        List<ImageEntity> imageEntityList = Arrays.asList(imageEntity1, imageEntity2);
        return new PageImpl<>(imageEntityList, pageable, imageEntityList.size());
    }

    ModelMapper createModelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setFieldMatchingEnabled(true);
        return mapper;
    }
}
