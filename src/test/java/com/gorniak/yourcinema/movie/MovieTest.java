package com.gorniak.yourcinema.movie;

import com.gorniak.yourcinema.comment.Comment;
import com.gorniak.yourcinema.comment.CommentPrototype;
import com.gorniak.yourcinema.user.UserAccount;
import com.gorniak.yourcinema.user.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
public class MovieTest {

    static final String TEST_TITLE = "TestTitle";
    static final String TEST_DESCRIPTION = "TestDescription";
    static final String TEST_AUTHOR = "TestAuthor";
    static final String TEST_MESSAGE = "TestMessage";
    static final String COMMENT_NOT_EXIST_IN_COMMENTS_LIST_FROM_MOVIE = "Comment not exist in comments list from Movie";
    static final String TEST_EMAIL_USER_LOGGED = "testEmailUserLogged";
    static final String TEST_EMAIL = "test@test.pl";
    static final String TEST_PASSWORD = "test123";

    Movie movie;
    CommentPrototype commentPrototype;
    CommentPrototype commentPrototype2;
    Comment comment;
    Comment comment2;
    UserAccount userAccount;

    @BeforeEach
    void setUp() {
        movie = new Movie(TEST_TITLE, TEST_DESCRIPTION, MovieType.FANTASY, LocalDate.now(), 0.0);
        commentPrototype = new CommentPrototype(TEST_MESSAGE, 10.0);
        commentPrototype2 = new CommentPrototype(TEST_MESSAGE, 5.0);
        comment = new Comment(1L, commentPrototype, TEST_EMAIL_USER_LOGGED);
        comment2 = new Comment(2L, commentPrototype2, TEST_EMAIL_USER_LOGGED);
        userAccount = new UserAccount(TEST_EMAIL, TEST_PASSWORD, UserRole.USER);
    }

    @Test
    void addCommentShouldAddedNewCommentToListComments() {
        //given+when
        movie.addComment(comment);
        movie.addComment(comment2);

        //then
        assertThat(movie.countComments()).isEqualTo(2);
        assertThat(movie.getRating()).isEqualTo(7.5);
    }

    @Test
    void getCommentShouldReturnCommentWhenExistInSet() {
        //given
        movie.addComment(comment);

        //when
        Comment commentFind = movie.getComment(1);

        //then
        assertAll(
                () -> assertThat(commentFind.getAuthor()).isEqualTo(comment.getAuthor()),
                () -> assertThat(commentFind.getRating()).isEqualTo(comment.getRating()),
                () -> assertThat(commentFind.getMessage()).isEqualTo(comment.getMessage()),
                () -> assertThat(commentFind.getMovie()).isEqualTo(comment.getMovie()),
                () -> assertThat(commentFind.getCommentDateCreate()).isEqualTo(comment.getCommentDateCreate())
        );
    }

    @Test
    void getCommentShouldThrowExceptionWhenCommentNotExistInSet() {
        //given
        movie.addComment(comment2);

        //when+then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> movie.getComment(1)).withMessageContaining(COMMENT_NOT_EXIST_IN_COMMENTS_LIST_FROM_MOVIE);
    }

    @Test
    void subscribeReturnTrue() {
        //given+when
        boolean isSubscribe = movie.subscribe(userAccount);
        Set<UserAccount> subscribers = movie.getSubscribers();

        //then
        assertTrue(isSubscribe);
        assertTrue(subscribers.contains(userAccount));
    }

    @Test
    void subscribeReturnFalse() {
        //given+when
        movie.subscribe(userAccount);
        boolean isSubscribe = movie.subscribe(userAccount);
        Set<UserAccount> subscribers = movie.getSubscribers();

        //then
        assertFalse(isSubscribe);
    }

    @Test
    void unsubscribeReturnTrue() {
        //given
        boolean isSubscribe = movie.subscribe(userAccount);

        //when
        boolean isUnsubscribe = movie.unsubscribe(userAccount);
        Set<UserAccount> subscribers = movie.getSubscribers();

        //then
        assertTrue(isSubscribe);
        assertTrue(isUnsubscribe);
        assertThat(subscribers.size()).isEqualTo(0);
    }

    @Test
    void unsubscribeReturnFalse() {
        //when+given
        boolean isUnsubscribe = movie.unsubscribe(userAccount);
        Set<UserAccount> subscribers = movie.getSubscribers();

        //then
        assertFalse(isUnsubscribe);
        assertThat(subscribers.size()).isEqualTo(0);
    }
}
