package com.gorniak.yourcinema.movie;

import com.gorniak.yourcinema.comment.CommentPrototype;
import com.gorniak.yourcinema.dataproviders.movie.MovieProvider;
import com.gorniak.yourcinema.dataproviders.user.UserAccountProvider;
import com.gorniak.yourcinema.error.exception.NameCanBeUniqueException;
import com.gorniak.yourcinema.user.UserAccount;
import com.gorniak.yourcinema.user.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.STRICT_STUBS)
@ExtendWith(MockitoExtension.class)
class MovieProcessorTest {

    static final String TEST_TITLE = "TestTitle1";
    static final String TEST_DESCRIPTION = "TestDescription1";
    static final String TEST_AUTHOR = "TestAuthor";
    static final String TEST_MESSAGE = "TestMessage";
    static final String TEST_EMAIL_USER_LOGGED = "testEmailUserLogged";
    static final String TEST_PASSWORD = "test123";

    @Mock
    MovieProvider movieProvider;

    @Mock
    UserAccountProvider userAccountProvider;

    @InjectMocks
    MovieProcessor movieProcessor;

    Movie movie;
    UserAccount userAccount;

    @BeforeEach
    void setUp() {
        movie = new Movie(TEST_TITLE, TEST_DESCRIPTION, MovieType.FANTASY, LocalDate.now(), 10.0);
        userAccount = new UserAccount(TEST_EMAIL_USER_LOGGED, TEST_PASSWORD, UserRole.USER);
    }

    @Test
    void getMoviesShouldReturnListMovies() {
        //given
        Pageable pageable = PageRequest.of(0, 5);
        when(movieProvider.getMovies(pageable)).thenReturn(createTestPage(pageable, movie));

        //when
        List<Movie> movies = movieProcessor.getMovies(pageable);

        //then
        assertAll(
                () -> assertThat(movies.size()).isEqualTo(1),
                () -> assertThat(movies.get(0).getTitle()).isEqualTo(TEST_TITLE),
                () -> assertThat(movies.get(0).getDescription()).isEqualTo(TEST_DESCRIPTION),
                () -> assertThat(movies.get(0).getYearProduction().getMonth()).isEqualTo(LocalDate.now().getMonth()),
                () -> assertThat(movies.get(0).getMovieType()).isEqualTo(MovieType.FANTASY)
        );
        verify(movieProvider, times(1)).getMovies(pageable);
    }

    @Test
    void getMovieShouldReturnMovie() {
        //given
        when(movieProvider.getMovie(TEST_TITLE)).thenReturn(movie);

        //when
        Movie movieReturnToMethod = movieProcessor.getMovie(TEST_TITLE);

        //then
        assertAll(
                () -> assertThat(movieReturnToMethod.getTitle()).isEqualTo(TEST_TITLE),
                () -> assertThat(movieReturnToMethod.getDescription()).isEqualTo(TEST_DESCRIPTION),
                () -> assertThat(movieReturnToMethod.getYearProduction().getMonth()).isEqualTo(LocalDate.now().getMonth()),
                () -> assertThat(movieReturnToMethod.getMovieType()).isEqualTo(MovieType.FANTASY)
        );
        verify(movieProvider, times(1)).getMovie(TEST_TITLE);
    }

    @Test
    void saveMovieOkTest() {
        //given
        when(movieProvider.exist(TEST_TITLE)).thenReturn(false);

        //when
        movieProcessor.createMovie(movie);

        //then
        verify(movieProvider, times(1)).exist(TEST_TITLE);
        verify(movieProvider, times(1)).saveMovie(movie);
    }

    @Test
    void saveMovieShouldReturnExceptionWhenMovieWithTitleExist() {
        //given
        when(movieProvider.exist(TEST_TITLE)).thenReturn(true);

        //when+then
        assertThatExceptionOfType(NameCanBeUniqueException.class)
                .isThrownBy(() -> movieProcessor.createMovie(movie)).withMessageContaining(TEST_TITLE);
        verify(movieProvider, times(1)).exist(TEST_TITLE);
        verify(movieProvider, times(0)).saveMovie(movie);
    }

    @Test
    void createCommentOkTest() {
        //given
        CommentPrototype commentPrototype = new CommentPrototype(TEST_MESSAGE, 10.0);
        when(movieProvider.getMovie(TEST_TITLE)).thenReturn(movie);

        //when
        movieProcessor.addComment(TEST_TITLE, commentPrototype, TEST_EMAIL_USER_LOGGED);

        //then
        verify(movieProvider, times(1)).getMovie(TEST_TITLE);
        verify(movieProvider, times(1)).saveMovie(any(Movie.class));
    }

    Page<Movie> createTestPage(Pageable pageable, Movie movie) {
        List<Movie> movies = Collections.singletonList(movie);
        return new PageImpl<>(movies, pageable, movies.size());
    }

    @Test
    void subscribeMovieShouldReturnTrue(){
        //given
        when(movieProvider.getMovie(TEST_TITLE)).thenReturn(movie);
        when(userAccountProvider.getUserAccount(TEST_EMAIL_USER_LOGGED)).thenReturn(userAccount);

        //when
        Boolean isSubscribeMovie = movieProcessor.subscribeMovie(TEST_TITLE, TEST_EMAIL_USER_LOGGED);

        //then
        assertTrue(isSubscribeMovie);
        assertTrue(movie.getSubscribers().contains(userAccount));
        verify(movieProvider ,times(1)).saveMovie(movie);
    }

    @Test
    void unSubscribeMovieReturnTrue(){
        //given
        when(movieProvider.getMovie(TEST_TITLE)).thenReturn(movie);
        when(userAccountProvider.getUserAccount(TEST_EMAIL_USER_LOGGED)).thenReturn(userAccount);

        //when
        movie.subscribe(userAccount);
        Boolean isUnsubscribeMovie = movieProcessor.unsubscribeMovie(TEST_TITLE, TEST_EMAIL_USER_LOGGED);

        //then
        assertTrue(isUnsubscribeMovie);
        assertThat(movie.getSubscribers().size()).isEqualTo(0);
        verify(movieProvider, times(1)).saveMovie(movie);
    }

    @Test
    void unSubscribeMovieReturnFalse(){
        //given
        when(movieProvider.getMovie(TEST_TITLE)).thenReturn(movie);
        when(userAccountProvider.getUserAccount(TEST_EMAIL_USER_LOGGED)).thenReturn(userAccount);

        //when
        Boolean isUnsubscribeMovie = movieProcessor.unsubscribeMovie(TEST_TITLE, TEST_EMAIL_USER_LOGGED);

        //then
        assertFalse(isUnsubscribeMovie);
        assertThat(movie.getSubscribers().size()).isEqualTo(0);
        verify(movieProvider, times(0)).saveMovie(movie);
    }

}
