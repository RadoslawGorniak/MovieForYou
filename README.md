# MovieForYou

Web Service using REST dedicated to film and cinema star.

## Table of contents

* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Code Examples](#code_examples)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)
* [License](#license)


## General info

The assumption of the application is to create a place where every user will have access to a database of available films and premieres. Thanks to the commenting, rating and subscribing system, the user will be able to choose more interesting movie titles recommended by other users.



## Technologies

* Java 11
* Spring Boot 2
* Hibernate
* Liquibase
* PostgresSql
* H2
* JUnit 5
* Mockito 2
* Swagger 2
* Cloudinary API
* Jacoco
* REST
* SOLID

## Setup

Clone the repository on your computer.
Go from the terminal to the directory where the downloaded repository is located and execute the following commands

```bash
mvn clean install
mvn spring-boot:run
```

Then the application is available under [localhost:8080/movies](http://localhost:8080/movies)

## Code Examples

* Registartion user:

![registration](./img/registartion.png)

* Add movie with title Titanic:

![add_movie](./img/add_movie.png)

* Find all movie:

![all_movie](./img/all_movie.png)

* Find one movie with title Titanic:

![find_one_movie](./img/find_one_movie.png)

* Add Comment to movie with title Titanic

![add_comment](./img/add_comment.png)

* Find first comment from movie with title Titanic

![find_1_comment_from_movie](./img/find_1_comment_from_movie.png)

* Add image to movie with title Titanic

![add_image_to_movie](./img/add_image_to_movie.png)

* Find all images from movie with title Titanic

![get_image_from_movie](./img/get_image_from_movie.png)

* Subscribe movie with title Titanic

![subscribe_movie](./img/subscribe_movie.png)

* Unsubscribe movie with title Titanic

![unsubscribe_movie](./img/unsubscribe_movie.png)



## Features

List of features ready and TODOs for future development
* User registration
* Global error handling
* Movie search engine by title
* Adding comments and ratings / user
* Adding photos to movies / admin
* List of all users / admin
* List of all movies
* List of movie comments
* List of photos from the movie
* Movie subscription available and unsubscribing
* Movie subscribers lists
* List of user's favorite movies
* adding movies and photos / admin


To-do list:
* Addition of film premieres,
* Addition of top10 movies,
* Addition of profanity validator

## Status

Project is: In progress

## Contact

Created by [RadosławGórniak](https://www.linkedin.com/in/radoslaw-gorniak-b6b4b2172/) - feel free to contact me!

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
